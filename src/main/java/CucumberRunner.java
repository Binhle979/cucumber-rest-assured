import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
@CucumberOptions(
        features = "src/main/resources/TestAPI.feature"
)
public class CucumberRunner extends AbstractTestNGCucumberTests {

}
