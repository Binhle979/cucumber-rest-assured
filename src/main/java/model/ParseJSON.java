package model;

import com.google.gson.Gson;

public class ParseJSON {
    public String parseJsonToString(PostBody postBody){
        return new Gson().toJson(postBody);
    }
}
