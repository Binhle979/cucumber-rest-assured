package model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
@Builder
@Getter
@Setter
@ToString

public class PostBody {
    private int userId;
    private int id;
    private String title;
    private String body;
}
