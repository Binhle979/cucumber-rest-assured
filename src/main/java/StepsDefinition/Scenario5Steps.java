package StepsDefinition;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import model.ApiMethod;
import org.testng.Assert;

import java.util.List;
import java.util.Map;

public class Scenario5Steps {
    ApiMethod apiMethod = new ApiMethod();

    @Given("Create a GET request to the web thesportsdb.com with endpoint:")
    public void createAGETRequestToTheWebThesportsdbComWithEndpoint(DataTable dataTable) {
        List<Map<String,String>> data = dataTable.asMaps(String.class,String.class);
        apiMethod.setUpRequest();
        apiMethod.methodGET(data.get(0).get("endpoint"));
    }

    @Then("the request success with the name of event is displayed")
    public void theRequestSuccessWithTheNameOfPlayerIsDisplayed(DataTable dataTable) {
        List<Map<String,String>> data = dataTable.asMaps(String.class,String.class);
        List<String> jsonData = apiMethod.jsonData("$..strEvent");
        System.out.println(jsonData);
        Assert.assertTrue(jsonData.contains(data.get(0).get("event")));
    }
}
