package StepsDefinition;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import model.ApiMethod;

import java.util.List;
import java.util.Map;

public class Scenario1Steps {

    ApiMethod apiMethod = new ApiMethod();

    @Given("Create a GET request to thesportsdb.com with endpoint")
    public void makeRequestGET(DataTable dataTable){
        List<Map<String,String>> data=dataTable.asMaps(String.class,String.class);
        String endpoint =data.get(0).get("endpoint");
        apiMethod.setUpRequest();
        apiMethod.methodGET(endpoint);
    }

    @Then("status code response is {int} and Will displayed sport name:")
    public void verifyStatusCodeAndItem(int statusCode ,DataTable dataTable) {
        apiMethod.verifyStatusCode(statusCode);
        List<String> jsonData = apiMethod.jsonData("$..strSport");
        System.out.println(jsonData);
        List<Map<String,String>> data=dataTable.asMaps(String.class,String.class);
        for (int i=0;i<dataTable.height()-1;i++){
            String value = data.get(i).get("sport name");
            jsonData.contains(value);
        }

    }
}
