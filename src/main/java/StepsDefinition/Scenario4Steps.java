package StepsDefinition;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import model.ApiMethod;

import java.util.List;
import java.util.Map;

public class Scenario4Steps {
    ApiMethod apiMethod = new ApiMethod();

    @Given("Create a GET request to the website thesportsdb.com with endpoint:")
    public void createAGETRequestToTheWebsiteThesportsdbComWithEndpoint(DataTable dataTable) {
        List<Map<String,String>> data = dataTable.asMaps(String.class,String.class);
        apiMethod.setUpRequest();
        apiMethod.methodGET(data.get(0).get("endpoint"));
    }


    @Then("the request success with the {string} is displayed")
    public void theRequestSuccessWithTheIdLeagueIsDisplayed(String idLeague) {
        List<String> jsonData = apiMethod.jsonData("$.leagues");
        System.out.println(jsonData);
        jsonData.contains(idLeague);
    }
}
