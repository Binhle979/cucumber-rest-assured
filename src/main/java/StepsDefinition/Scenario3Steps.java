package StepsDefinition;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import model.ApiMethod;
import org.testng.Assert;

import java.util.List;
import java.util.Map;

public class Scenario3Steps {

    ApiMethod apiMethod = new ApiMethod();

    @Given("Create a GET request to the website thesportsdb.com with endpoint all Leagues in England")
    public void createAGETRequestToTheWebsiteThesportsdbComWithEndpoint(DataTable dataTable) {
        List<Map<String,String>> data = dataTable.asMaps(String.class,String.class);
        apiMethod.setUpRequest();
        apiMethod.methodGET(data.get(0).get("endpoint"));
    }

    @Then("status code response is {int} and will display {int} leagues")
    public void statusCodeResponseIsAndWillDisplayedLeagues(int statusCode,int numberOfLeagues) {
        apiMethod.verifyStatusCode(statusCode);
        List<String> jsonData = apiMethod.jsonData("$..strLeagueAlternate");
        System.out.println(jsonData.size());
        Assert.assertEquals(jsonData.size(),numberOfLeagues);
    }

    @And("The result have must strCountry tag with value is {string} and the result have must tags include:")
    public void theResultHaveMustStrCountryTagWithValueIsEnglandAndTheResultHaveMustTagsInclude(String country,DataTable dataTable) {
        List<Map<String,String>> data=dataTable.asMaps(String.class,String.class);
        List<String> jsonData = apiMethod.jsonData("$..strCountry");
        Assert.assertTrue(jsonData.contains(country));
        List<String> jsonDataCountry = apiMethod.jsonData("$.countries");
        for (int i=0;i<dataTable.height()-1;i++){
            String value = data.get(i).get("tags");
            System.out.println(value);
            Assert.assertTrue(jsonDataCountry.toString().contains(value));
        }
    }
}
