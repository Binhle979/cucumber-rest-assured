package StepsDefinition;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import model.ApiMethod;
import org.testng.Assert;

import java.util.List;
import java.util.Map;

public class Scenario2Steps {

    ApiMethod apiMethod = new ApiMethod();

    @Given("Create a GET request to the website thesportsdb.com with endpoint")
    public void createAGETRequestToTheWebsiteThesportsdbComWithEndpoint(DataTable dataTable) {
        List<Map<String,String>> data = dataTable.asMaps(String.class,String.class);
        apiMethod.setUpRequest();
        apiMethod.methodGET(data.get(0).get("endpoint"));
    }

    @Then("status code response is {int} and will display {int} leagues include:")
    public void statusCodeResponseIsAndWillDisplayedLeagues(int statusCode,int numberOfLeagues,DataTable dataTable) {
        apiMethod.verifyStatusCode(statusCode);
        List<String> jsonData = apiMethod.jsonData("$..strLeagueAlternate");
        System.out.println(jsonData);
        Assert.assertEquals(jsonData.size(),numberOfLeagues);
        List<Map<String,String>> data=dataTable.asMaps(String.class,String.class);
        for (int i=0;i<dataTable.height()-1;i++){
            String value = data.get(i).get("leagues");
            Assert.assertTrue(jsonData.toString().contains(value));
        }
    }
}
