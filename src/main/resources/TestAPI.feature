Feature: Test API with Rest Assured

  Scenario: The URL will be returning all sports.
    Given Create a GET request to thesportsdb.com with endpoint
      | endpoint        |
      | /all_sports.php |
    Then status code response is 200 and Will displayed sport name:
      | sport name |
      | Soccer     |
      | Motorsport |
      | Fighting   |
      | Baseball   |
      | Basketball |

  Scenario: The URL will be return all leagues
    Given Create a GET request to the website thesportsdb.com with endpoint
      | endpoint         |
        | /all_leagues.php |
    Then status code response is 200 and will display 924 leagues include:
      | leagues                |
      | Premier League |
      | Bundesliga      |
      | Serie A        |

  Scenario: The URL will be return all Leagues in a country
    Given Create a GET request to the website thesportsdb.com with endpoint all Leagues in England
      | endpoint                          |
      | /search_all_leagues.php?c=England |
    Then status code response is 200 and will display 31 leagues
    And The result have must strCountry tag with value is "England" and the result have must tags include:
      | tags           |
      | idLeague       |
      | strSport       |
      | strLeague      |
      | dateFirstEvent |
      | strCountry     |

  Scenario: The URL will be return All Leagues
    Given Create a GET request to the website thesportsdb.com with endpoint:
      | endpoint         |
      | /all_leagues.php |
    Then the request success with the "idLeague" is displayed

  Scenario: The URL will be return the name of the player
    Given Create a GET request to the web thesportsdb.com with endpoint:
      | endpoint                               |
      | /searchevents.php?e=Arsenal_vs_Chelsea |
    Then the request success with the name of event is displayed
      | event        |
      | Arsenal vs Chelsea |